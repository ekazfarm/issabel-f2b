<h3>Issabel PBX-Gui Fail2Ban</h4>

<p>This is script if you want to block user that wrong password from webgui, normally issabel will block only admin users, but with this script all users can be blocked</p>



# Fail2Ban filter for Issabel GUI Failed Logins
#

[INCLUDES]

# Read common prefixes. If any customizations available -- read them from
# common.local
before = common.conf

[Definition]

_daemon = asterisk

__pid_re = (?:\[\d+\])

iso8601 = \d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+[+-]\d{4}

failregex = ^(%(__prefix_line)s|\[\]\s*)LOGIN ([^:]*): Authentication Failure to Web Interface login. Failed password for admin from <HOST>.$
 	   ^(%(__prefix_line)s|\[\]\s*)LOGIN ([^:]*): .*Authentication Failure to Web Interface login. Invalid user .* from <HOST>.$

ignoreregex =


<p>-----------------------------------------------------------------------------------------------------</p>

If you want script for unban the ip you can search on my repo "f2b-release"
Thanks
